<?php
  switch(@$params[0])
  {
    case 'usun':
      $id_do_usuniecia = $params[1];
      if($db->query('delete from kategorie where id_kategoria=' . $id_do_usuniecia))
      {
        header("Location: /tablica/adminpanel/kategorie");
      }
      break;

    case 'edytuj':
      $id_do_edycji = $params[1];
      if($db->query('update kategorie set nazwa_kategorii="' . $_POST['nazwa'] . '" where id_kategoria=' . $id_do_edycji))
      {
        header("Location: /tablica/adminpanel/kategorie");
      }
      break;

    case 'dodaj':
      if($db->query('insert into kategorie values(null, "' . $_POST['nazwa'] . '")'))
      {
        header("Location: /tablica/adminpanel/kategorie");
      }
      break;

  }



  $kategorie_query = 'select * from kategorie order by nazwa_kategorii;';
  $kategorie_array = $db->query($kategorie_query);
?>
<form method="post" class="ui form" action="./adminpanel/kategorie/dodaj">
  <div class="inline fields">
      <div class="eight wide field">
        <input type="text" name="nazwa" placeholder="Nazwa kategorii" />
      </div>
      <div class="field">
        <button type="submit" class="ui green labeled icon button">
          <i class="add icon"></i>
          Dodaj nową kategorię
        </button>
      </div>
  </div>
</form>

<table class="ui celled table">
  <thead>
    <tr>
      <th>Nazwa kategorii</th>
      <th></th>
    </tr>
  </thead>
<?php
  foreach($kategorie_array as $k)
  {
    echo '<tr>';
    echo '<td>' . $k['nazwa_kategorii'] . '</td>';
    echo '<td class="right aligned collapsing">';
    echo '<a onclick="$(\'.ui.modal.do-edycji.' . $k['id_kategoria'] . '\').modal(\'show\');" class="ui primary labeled icon button"><i class="pencil icon"></i>edytuj</a>
          <a onclick="$(\'.ui.basic.modal.do-usuniecia.' . $k['id_kategoria'] . '\').modal(\'show\');" class="ui red labeled icon button"><i class="trash icon"></i>usuń</a>';
    echo '<div class="ui basic modal do-usuniecia ' . $k['id_kategoria'] . '">
            <div class="ui icon header">
              <i class="trash alternate icon"></i>
              Usunąć kategorię "' . $k['nazwa_kategorii'] . '"?
            </div>
            <div class="content">
              <p>Operacja jest nieodwracalna.</p>
            </div>
            <div class="actions">
              <div class="ui green cancel inverted button">
                <i class="remove icon"></i>
                Nie
              </div>
              <a href="./adminpanel/kategorie/usun/' . $k['id_kategoria'] . '" class="ui red labeled icon ok button">
                <i class="trash alternate icon"></i>
                Tak
              </a>
            </div>
          </div>


          <form class="ui form modal do-edycji ' . $k['id_kategoria'] . '" action="./adminpanel/kategorie/edytuj/' . $k['id_kategoria'] . '" method="post">
            <div class="header">
              Zmień nazwę dla "' . $k['nazwa_kategorii'] . '"
            </div>
            <div class="content">
              <input type="text" name="nazwa" placeholder="Nowa nazwa..." />
            </div>
            <div class="actions">
              <div class="ui black deny button">
                Anuluj
              </div>
              <button type="submit" class="ui positive right labeled icon button">
                Zapisz
                <i class="save icon"></i>
              </div>
            </div>
          </form>';

    echo '</td>';
    echo '</tr>';
  }
?>
</table>

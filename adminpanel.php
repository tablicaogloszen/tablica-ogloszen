<?php
  $pozycje_menu = array(array('alias' => 'kategorie', 'nazwa' => 'Kategorie'), array('alias' => 'uzytkownicy', 'nazwa' => 'Użytkownicy'), array('alias' => 'zgloszenia', 'nazwa' => 'Zgłoszenia'));
  if(!empty($params[0]))
  {
    $active = array_shift($params);
  }
  else
  {
    $active = 'kategorie';
  }
?>
<article>
  <h1 class="ui dividing header">Panel Administracyjny</h1>
  <div class="ui two column stackable grid">
    <div class="four wide column">
      <div class="ui secondary vertical pointing menu">
        <?php
          foreach($pozycje_menu as $men)
          {
            echo '<a class="';
            if($men['alias'] == $active) echo 'active ';
            echo 'item" href="./adminpanel/';
            echo $men['alias'];
            echo '">';
            echo $men['nazwa'];
            echo '</a>';
          }
         ?>
      </div>
    </div>
    <div class="twelve wide column">
      <?php include 'adminpanel.' . $active . '.php'; ?>
    </div>
  </div>
</article>

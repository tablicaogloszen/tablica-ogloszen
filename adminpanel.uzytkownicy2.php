<?php
$q_s_users = 'select * from users order by username;';
$q_users = $db->query($q_s_users);

?>
<h4 style="ui header">Blokowanie/odblokowywanie użytkownika</h4>
<form method="post" class="ui form" action="./adminpanel/manage_user">
  <div class="inline fields">
  <div class="wide field">
      <select class="ui dropdown" name="wybor_usera">
	  <option disabled selected value>Wybierz użytkownika do zarządzania</option>
		<?php
			foreach($q_users as $u)
			{
				if($u['admin']!= 1)
				{
					echo '<option value="'.$u['id_user'].'"';
					echo '>'.$u['username'].'</option>';
				}
			}
		?>
	</select>
	<div class="field">
	<select class="ui dropdown" name="wybor_akcji">
		<option disabled selected value>Wybierz akcję</option>
		<option value="0">Odblokuj użytkownika</option>
		<option value="-1">Zablokuj użytkownika</option>
		<option value="1">Przyznaj uprawnienia administratora</option>
	</select>
	</div>
      <div class="field">
        <button type="submit" class="ui green labeled icon button">
          <i class="pencil icon"></i>
          Zarządzaj użytkownikiem
        </button>
      </div>
  </div>
  </div>
</form>
<?php
if(isset($params[0]) && $params[0] == 'manage_user')
	{
		//print_r($_POST['wybor_akcji']);
		$db->query('update users set admin ='.$_POST['wybor_akcji'].' where id_user= "' . $_POST['wybor_usera']. '"');
		header('Location: /tablica/adminpanel');
	}
?>
<h4 style="ui header">Usuń użytkownika</h4>
<form method="post" class="ui form" action="./adminpanel/del_user">
  <div class="inline fields">
  <div class="eight wide field">
      <select class="ui dropdown" name="wybor_usera">
	  <option disabled selected value>Wybierz użytkownika do usunięcia</option>
		<?php
			foreach($q_users as $u)
			{
				if($u['admin']!= 1)
				{
					echo '<option value="'.$u['id_user'].'"';
					echo '>'.$u['username'].'</option>';
				}
			}
		?>
	</select>
      <div class="field">
        <button type="submit" class="ui red labeled icon button">
          <i class="delete icon"></i>
          Usuń użytkownika
        </button>
      </div>
  </div>
  </div>
</form>
<?php
if(isset($params[0]) && $params[0] == 'del_user')
	{
		//print_r($_POST['wybor_kategoria']);
		$db->query('delete from users where id_user = "' . $_POST['wybor_usera']. '"');
		header('Location: /tablica/adminpanel');
	}
?>
<h4 style="ui header">Zmiana hasła użytkownika</h4>
<form method="post" class="ui form" action="./adminpanel/change_password">
  <div class="inline fields">
  <div class="eight wide field">
      <select class="ui dropdown" name="wybor_usera">
	  <option disabled selected value>Wybierz użytkownika, któremu chcesz zmienić hasło</option>
		<?php
			foreach($q_users as $u)
			{
				if($u['admin']!= 1)
				{
					echo '<option value="'.$u['id_user'].'"';
					echo '>'.$u['username'].'</option>';
				}
			}
		?>
	</select>
		<div class="field">
			<input type="password" name="haslo" placeholder="Podaj nowe hasło" />
		</div>
		<div class="field">
			<input type="password" name="haslo2" placeholder="Powtórz nowe hasło" />
		</div>
      <div class="field">
        <button type="submit" class="ui blue labeled icon button">
          <i class="pencil icon"></i>
          Zmień hasło użytkownikowi
        </button>
      </div>
  </div>
  </div>
</form>
<?php
if(isset($params[0]) && $params[0] == 'change_password')
	{
		if($_POST['haslo']==$_POST['haslo2']){
			$password = md5($_POST['haslo']);
		//print_r($_POST['wybor_kategoria']);
			$db->query('update users set password="'.$password.'" where id_user = "' . $_POST['wybor_usera']. '"');
			echo '<script> alert("Hasło zostało zmienione")</script>';
			//header('Location: /tablica/adminpanel');

		}
		else
		{
				//echo '<h1>Hasła się różnią!</h1>';
				echo '<script> alert("Podane hasła do siebie nie pasują. Hasło użytkownika nie zostało zmienione")</script>';
		}
	}
?>

<?php
$zgloszenia = 'select zgloszenia.*, users.username from zgloszenia join users on zgloszenia.zgloszony=users.id_user';
$z = $db->query($zgloszenia);
?>
<br />
<table class="ui celled table">
  <thead>
    <tr>
	<th>Nick zgłoszonego</th>
    <th>Powód</th>
    <th>Czy zrobione</th>
	<th>Kto rozwiązał</th>
	<th>Kiedy wykonano?</th>
  </tr>
  </thead>
  <?php
  foreach($z as $zg)
  {
	if($zg['czy_zalatwione']==0){
		echo '<tr>';
	}
	else
	{
			echo '<tr class="active">';
	}
	echo '<td data-label="Nick">'.$zg['username'].'</td>';
	echo '<td data-label="Powód">'.$zg['powod'].'</td>';
	if($zg['czy_zalatwione']==1)
		{
			echo '<td data-label="Czy zrobione"><a href="./adminpanel/zgloszenia/niezrobione/'.$zg['id_zgloszenia'].'"><i class="check icon"></i></a></td>';
		}
	else
	{
		echo '<td data-label="Czy zrobione"><a href="./adminpanel/zgloszenia/zrobione/'.$zg['id_zgloszenia'].'"><i class="close icon"></i></a></td>';
	}
	if($zg['fk_id_admina']!=0)
		{
			$usernick='select username as u from users where id_user='.$zg['fk_id_admina'];
			$kto = $db->query($usernick);
			$ktorozwiazal = $kto->fetch_array(MYSQLI_ASSOC);
			$who = $ktorozwiazal['u'];
			echo '<td data-label="kto_rozwiazal">'.$who.'</td>';}
	else
		{
			echo '<td data-label="kto_rozwiazal">-</td>';
		}
	if($zg['data_wykonania'] == '0000-00-00 00:00:00')
	{
		echo '<td data-label="kiedy">-</td>';
	}
	else
	{
		echo '<td data-label="kiedy">'.$zg['data_wykonania'].'</td>';
	}
	echo '</tr>';
	}
  ?>
 </table>
<?php
if(isset($params[0]) && $params[0] == 'zrobione')
	{
		//print_r($_POST['wybor_kategoria']);
		$data = date("Y-m-d H:i:s");
		$db->query('update zgloszenia set czy_zalatwione="1" where id_zgloszenia='.$params[1]);
		$db->query('update zgloszenia set data_wykonania="'.$data.'" where id_zgloszenia='.$params[1]);
		$db->query('update zgloszenia set fk_id_admina="'.$_SESSION['id_uzytkownika'].'" where id_zgloszenia='.$params[1]);
		header('Location: /tablica/adminpanel/zgloszenia');
	}
if(isset($params[0]) && $params[0] == 'niezrobione')
	{
		//print_r($_POST['wybor_kategoria']);
		$db->query('update zgloszenia set czy_zalatwione="0" where id_zgloszenia='.$params[1]);
		$db->query('update zgloszenia set fk_id_admina="0" where id_zgloszenia='.$params[1]);
		$db->query('update zgloszenia set data_wykonania="0000-00-00 00:00:00" where id_zgloszenia='.$params[1]);
		header('Location: /tablica/adminpanel/zgloszenia');
	}
?>

<?php
if(count($params) > 1)
{
  $action = array_shift($params);
  if($action == 'przenies')
  {
    $id = array_shift($params);
    $query = 'select * from ogloszenie where id_ogloszenie=' . $id;
    if($stare = $db->query($query)->fetch_array(MYSQLI_ASSOC))
    {
      $nowe_query = 'insert into archiwum values("' . implode('", "', $stare) . '");';
      if($db->query($nowe_query))
      {
        $db->query('delete from ogloszenie where id_ogloszenie='.$id);
        header("Location: /tablica/archiwum/" . $id);
      }
    }
  }
}
elseif(!empty($params[0]))
{
  $id = array_shift($params);
  $ogloszenie_query = 'select archiwum.*, users.username, users.email, kategorie.nazwa_kategorii, wojewodztwa.nazwa_wojewodztwa from archiwum join users on archiwum.fk_id_user = users.id_user join kategorie on archiwum.fk_id_kategoria=kategorie.id_kategoria join wojewodztwa on archiwum.fk_id_wojewodztwa=wojewodztwa.id_wojewodztwa where id_ogloszenie=' . $id;
  $ogloszenie = $db->query($ogloszenie_query);
  if($ogloszenie->num_rows)
  {
    $ogloszenie = $ogloszenie->fetch_array(MYSQLI_ASSOC);
    $nazwa = $ogloszenie['nazwa'];
  	$opis = $ogloszenie['opis'];
  	$cena = $ogloszenie['cena'];
  	$id_kategorii = $ogloszenie['fk_id_kategoria'];
  	$nazwa_kategorii = $ogloszenie['nazwa_kategorii'];
  	$data_wystawienia = $ogloszenie['data_wystawienia'];
  	$uzytkownik = $ogloszenie['username'];
  	$email = $ogloszenie['email'];
  	$id = $ogloszenie['id_ogloszenie'];
  	$miasto = $ogloszenie['miasto'];
  	$wojewodztwo = $ogloszenie['nazwa_wojewodztwa'];
  	$dataPolska = convertDate($data_wystawienia);
  	$userid = $ogloszenie['fk_id_user'];
?>
<div class="ui icon blue message">
  <i class="archive icon"></i>
  <div class="content">
    <div class="header">
      Archiwum
    </div>
    <p>Ogłoszenie jest nieaktualne.</p>
  </div>
</div>
<article class="ui piled segment">
	<div class="ui two column stackable grid">
	<div class="twelve wide column">
		<h1 class="ui dividing header"><?php echo $nazwa; ?>
			<div class="ui right floated buttons">
				<button class="positive disabled ui button"><i class="pencil icon"></i> Zapytaj o przedmiot</button>
			</div>
		</h1>
		<?php
		/*if(($zalogowany && $uzytkownik == $_SESSION['id_uzytkownika'])|| ($zalogowany && $_SESSION['admin'] == 1)){
			echo '<a href="edytuj/' . $id . '" class="ui icon button right floated"><i class="pencil icon"></i></a>';
		}*/
		?>
		<p><?php echo $opis; ?></p>
		<?php
		if(isArticleImage($id))
		{
			$directory = 'images/ogloszenia/' . $id . '/';
			//$images = scandir($directory);
			$images = array_diff(scandir($directory), array('.', '..', 'thumbnail'));
		echo '<div class="ui bordered images galeria">';
			foreach($images as $image)
			{
				//echo '<a href="' . $directory . $image . '">';
				echo '<img data-src="'.$directory.$image.'" src="' . $directory;
				if(is_file($directory . 'thumbnail/' . $image)) echo 'thumbnail/';
				echo $image . '"/>';
				//echo '</a>';
			}
		echo '</div>';
			}
		?>

	</div>
	<div class="four wide column">
		<div class="ui massive tag labels">
			<div class="ui yellow label">
			<?php
			if($cena == '0')
				echo 'Za darmo';
			else
				echo $cena . 'zł';
			?>
			</div>
		</div>


		<h3 class="ui header">Lokalizacja:<div class="sub header"><?php echo $miasto; ?> (<?php echo $wojewodztwo;?>)</div></h3>
		<h3 class="ui header">Kategoria:<div class="sub header"><a href="./kategoria/<?php echo $id_kategorii; ?>"><?php echo $nazwa_kategorii; ?></a></div></h3>
		<h3 class="ui header">Użytkownik:<div class="sub header"><?php echo ' <a href="./userprofil/'.$userid.'">' . $uzytkownik . ' (<a href="mailto:' . $email . '">' . $email . '</a>)</div></h3>';?>
		<h3 class="ui header">Dodano:<div class="sub header"><?php echo $dataPolska; ?></div></h3>


</div>
</div>
</article>

<script>
function myFunction(imgs) {
  var expandImg = document.getElementById("expandedImg");
  var imgText = document.getElementById("imgtext");
  expandImg.src = imgs.src;
  imgText.innerHTML = imgs.alt;
  expandImg.parentElement.style.display = "block";
}
</script>

<?php
  }
}
else
{
  $link_aktywna = 'archiwum/';

  $wszystkie_ogloszenia_query = 'select archiwum.*, users.username, users.email, kategorie.nazwa_kategorii, wojewodztwa.nazwa_wojewodztwa from archiwum join users on archiwum.fk_id_user = users.id_user join kategorie on archiwum.fk_id_kategoria=kategorie.id_kategoria join wojewodztwa on archiwum.fk_id_wojewodztwa=wojewodztwa.id_wojewodztwa';
  //echo $wszystkie_ogloszenia_query;
  $wszystkie_ogloszenia = $db->query($wszystkie_ogloszenia_query);
  $wszystkie = $wszystkie_ogloszenia->num_rows;

  $ilosc_na_strone = 6;
  $ilosc_stron = ceil($wszystkie / $ilosc_na_strone);

  $strona = !empty($params[0]) && is_numeric($params[0]) ? $params[0] : 1;

  $ogloszenia_query = 'select archiwum.*, users.username, users.email, kategorie.nazwa_kategorii, wojewodztwa.nazwa_wojewodztwa from archiwum join users on archiwum.fk_id_user = users.id_user join kategorie on archiwum.fk_id_kategoria=kategorie.id_kategoria join wojewodztwa on archiwum.fk_id_wojewodztwa=wojewodztwa.id_wojewodztwa order by data_wystawienia desc, id_ogloszenie desc limit ' . (($strona - 1) * $ilosc_na_strone) . ', ' . ($ilosc_na_strone);
  //echo $ogloszenia_query;
  $ogloszenia = $db->query($ogloszenia_query);

?>
<h1 class="ui header">
  <div class="content">
    Archiwum
    <div class="sub header">Ogłoszenia poniżej są nieaktualne</div>
  </div>
</h1>
<?php

if($wszystkie > 0 && $strona <= $ilosc_stron)
{
    echo '<div class="ui two column stackable grid">';

    include 'ogloszenia.archiwum.php';

    showPagination($ilosc_stron, $strona, $link_aktywna);

    echo '</div>';
}
else
{
  echo '<div class="ui segment"><h1>Brak wyników</h1></div>';
}

}
?>

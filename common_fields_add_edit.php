<?php

echo '<h1 style="ui header">Dodaj/edytuj ogłoszenie</h1>';
$q_s_kategorie = 'select * from kategorie;';
$q_kategorie = $db->query($q_s_kategorie);
$q_s_wojewodztwa = 'select * from wojewodztwa;';
$q_wojewodztwa = $db->query($q_s_wojewodztwa);

if(count($_POST) == 0)
{
	deleteAll('uploads');
}
if(isset($_POST['tytul'])){
	$nazwa = htmlspecialchars($_POST['tytul']);
	$opis = htmlspecialchars($_POST['opis']);
	$cena = $_POST['cena'];
	$miasto = ucfirst(htmlspecialchars($_POST['miasto']));
  if(!isset($data_wystawienia))
	 $data_wystawienia = date('Y-m-d-H-i-s');
	$id_kategorii = $_POST['wybor_kategoria'];
	$id_wojewodztwa = $_POST['wybor_wojewodztwa'];
	$id_uzytkownika = $_SESSION['id_uzytkownika'];
  $query = sprintf($query_format, $nazwa, $opis, $cena, $data_wystawienia, $id_kategorii, $id_uzytkownika, $miasto, $id_wojewodztwa);
	echo $query;
	if($db->query($query)){

		if($action == 'dodaj') $id = $db->insert_id;
		$dir = 'images/ogloszenia/' . $id;
		if(!is_dir($dir)) mkdir($dir, 0777);
		$source = 'uploads/';
		$destination = $dir . '/';
		$files = scandir($source);
		foreach ($files as $file) {
		  if (in_array($file, array(".",".."))) continue;
		  if (copy($source . $file, $destination . $file)) {
		    unlink($source . $file);
		  }
		}

		$obserwowane_query = 'select * from obserwowane';
		$obserwowane = $db->query($obserwowane_query);
		foreach($obserwowane as $o)
		{
			$wyniki = $db->query('select * from ogloszenie where id_ogloszenie="'.$id.'" and nazwa like "%' . $o['fraza'] . '%"');
			if($wyniki->num_rows > 0)
			{
				$w = $wyniki->fetch_assoc();
				$w_insert_query = 'insert into wiadomosci values(null, "Znaleziono oberwowane ogłoszenie. <a href=\"szczegoly/'.$id.'\">'.$w['nazwa'].'</a>", 1, '.$o['id_uzytkownika'].', 0, "'.date('Y-m-d H:i:s').'", "Obserwowane", 1)';
				//echo $w_insert_query;
				$db->query($w_insert_query);
			}
		}
		header('Location: /tablica');
	}
} elseif($action == 'dodaj') {
  $nazwa = '';
  $opis = '';
  $cena = '';
  $miasto = '';
  $id_wojewodztwa = -1;//17
}
$tekst_button = $action == 'dodaj' ? 'Dodaj ogłoszenie' : 'Zapisz zmiany';
?>

<form method="POST" class="ui form" enctype="multipart/form-data">
	<div class="ui error message"></div>
	<div class="required field">
	<label>Tytuł</label>
	<input name="tytul" type="text" value="<?=$nazwa?>" />
	</div>
	<div class="required field">
	<label>Województwo</label>
	<select class="ui dropdown" name="wybor_wojewodztwa">
		<?php
		if($id_wojewodztwa == -1)
		{
			echo '<option disabled selected value>nie podano</option>';
		}
			foreach($q_wojewodztwa as $w)
			{
				echo '<option value="'.$w['id_wojewodztwa'].'"';
				if($id_wojewodztwa == $w['id_wojewodztwa']) echo ' selected="selected"';
				echo '>'.$w['nazwa_wojewodztwa'].'</option>';
			}
		?>
	</select>
	</div>
	<div class="required field">
	<label>Miasto</label>
	<input name="miasto" type="text" value="<?=$miasto?>" />
	</div>
	<div class="required field">
	<label>Kategoria</label>
	<select class="ui dropdown" name="wybor_kategoria">
		<?php
			foreach($q_kategorie as $k)
			{
				echo '<option value="'.$k['id_kategoria'].'"';
				if($id_kategorii == $k['id_kategoria']) echo ' selected="selected"';
				echo '>'.$k['nazwa_kategorii'].'</option>';
			}
		?>
	</select>
</div>
	<div class="required field">
	<label>Cena</label>
	<input name="cena" type="text" value="<?=$cena?>" />
	</div>
	<div class="field">
	<label>Zdjęcia</label>

	<div id="dropzone" class="dropzone"></div>


	</div>
	<div class="required field">
	<label>Opis</label>
	<textarea name="opis"><?=$opis?></textarea>
	</div>
	<button type="submit" class="positive ui labeled icon button"><i class="save icon"></i><?=$tekst_button?></button>
  <?php
    if($action == 'edytuj')
		{
			echo '<button onclick="$(\'.ui.basic.modal.delete\').modal(\'show\');" class="negative ui labeled icon button right floated" type="button"><i class="trash icon"></i> Usuń</a>';
			echo '<button onclick="$(\'.ui.basic.modal.archive\').modal(\'show\');" class="primary ui labeled icon button right floated" type="button"><i class="archive icon"></i> Archiwizuj</a>';
		}
  ?>
</form>

<div class="ui basic modal delete">
	<div class="ui icon header">
		<i class="trash alternate icon"></i>
		Usunąć?
	</div>
	<div class="content">
		<p>Operacja jest nieodwracalna.</p>
	</div>
	<div class="actions">
		<div class="ui green cancel inverted button">
			<i class="remove icon"></i>
			Nie
		</div>
		<a href="<?php echo 'usun/' . $id; ?>" class="ui red labeled icon ok button">
			<i class="trash alternate icon"></i>
			Tak
		</a>
	</div>
</div>
<div class="ui basic modal archive">
	<div class="ui icon header">
		<i class="archive icon"></i>
		Przenieść do archiwum?
	</div>
	<div class="content">
		<p>Operacja jest nieodwracalna.</p>
	</div>
	<div class="actions">
		<div class="ui green basic cancel inverted button">
			<i class="remove icon"></i>
			Nie
		</div>
		<a href="<?php echo 'archiwum/przenies/' . $id; ?>" class="ui primary ok inverted button">
			<i class="checkmark icon"></i>
			Tak
		</a>
	</div>
</div>

<script>
Dropzone.options.myAwesomeDropzone = false;
Dropzone.autoDiscover = false;
$("div#dropzone").dropzone({

	url: "upload.php",
	acceptedFiles: "image/jpeg,image/png,image/gif",

	init: function() {
		dz = this;
        $.get('upload.php', function(data) {
            $.each(data, function(key,value){
                var mockFile = { name: value.name, dataURL: value.dataURL, size: value.size };
								dz.emit('addedfile', mockFile);
								//dz.emit('thumbnail', mockFile, mockFile.name);
								//dz.createThumbnailFromUrl(mockFile, mockFile.name);
								dz.createThumbnailFromUrl(mockFile, dz.options.thumbnailWidth, dz.options.thumbnailHeight, dz.options.thumbnailMethod, true,
									function (thumbnail) {
						        dz.emit('thumbnail', mockFile, thumbnail);
						    });
								dz.emit('complete', mockFile);
            });
        });
    },

	addRemoveLinks: true,
	removedfile: function(file) {
    var name = file.name;
    $.ajax({
        type: 'POST',
        url: 'delete_uploaded_file.php',
        data: "id="+name,
        dataType: 'html'
    });
		var _ref;
		return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
	}
});

$('.ui.form')
  .form({
    fields: {
      tytul: {
        identifier: 'tytul',
        rules: [
					{
            type   : 'empty',
            prompt : 'Tytuł nie może być pusty'
          },
					{
            type   : 'minLength[4]',
            prompt : 'Minimalna długość tytułu to {ruleValue} znaki'
          }
        ]
      },
      wybor_wojewodztwa: {
        identifier: 'wybor_wojewodztwa',
        rules: [
          {
            type   : 'minCount[1]',
            prompt : 'Proszę podać województwo'
          }
        ]
      },
      miasto: {
        identifier: 'miasto',
        rules: [
          {
            type   : 'empty',
            prompt : 'Proszę wpisać miasto'
          }
        ]
      },
      cena: {
        identifier: 'cena',
        rules: [
          {
            type   : 'empty',
            prompt : 'Proszę podać cenę'
          }
        ]
      },
      opis: {
        identifier: 'opis',
        rules: [
          {
            type   : 'minLength[10]',
            prompt : 'Opis musi zawierać minimum {ruleValue} znaków'
          }
        ]
      }
    }
  })
;
</script>

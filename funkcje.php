<?php
function getArticleImage($id)
{
	$directory = 'images/ogloszenia/' . $id .'/';
	if(is_dir($directory))
	{
		$images = array_diff(scandir($directory), array('.', '..', 'thumbnail'));
		if(count($images) > 0)
		{
			$first_image = array_shift($images);
			if(is_file($directory . 'thumbnail/' . $first_image))
				return $directory . 'thumbnail/' . $first_image;
			return $directory . $first_image;
		}
	}
	return 'images/ogloszenia/brak_zdjecia.png';
}

function isArticleImage($id)
{
	$dir = 'images/ogloszenia/'.$id;
	if(is_dir($dir))
	{
		$d = scandir($dir);
		if(count($d) > 2) return true;
	}
	return false;
}

function convertDate($d, $pattern = 'd.m.Y H:i')
{
  return date($pattern, strtotime($d));
}

function getUserAvatar($user = -1)
{
	$avatars_src = 'images/avatary/';
	$default_avatar = 'default.png';
	$candidate = $avatars_src . $user . '.png';

	if(is_file($candidate))
	{
		return $candidate;
	}
	return $avatars_src . $default_avatar;
}

function isComments($id)
{
	global $db;
	$query = 'select count(*) as c from komentarze where dla_kogo=' . $id;
	$q = $db->query($query)->fetch_array();
	if($q['c'] > 0)
		return true;
	return false;
}

function showCommentsRecursively($id, $nadrzedna = 0)
{
	global $db;
	if($nadrzedna == 0)
	{
		$query = 'select *, users.username from komentarze join users on komentarze.kto_dal=users.id_user where komentarze.dla_kogo="' . $id . '" and id_nadrzednego is null order by komentarze.data_wystawienia desc';
	}
	else
	{
		$query = 'select *, users.username from komentarze join users on komentarze.kto_dal=users.id_user where komentarze.dla_kogo="' . $id . '" and id_nadrzednego=' . $nadrzedna . ' order by komentarze.data_wystawienia asc';
	}

	$result = $db->query($query);

	foreach($result as $r)
	{
		echo '<div class="comment">';
			echo '<a class="avatar"><img src="' . getUserAvatar($r['username']) . '" /></a>';
			echo '<div class="content">';
				echo '<a class="author" href="./userprofil/'.$r['kto_dal'].'">' . $r['username'] . '</a>';
				echo '<div class="metadata"><span class="date">' . convertDate($r['data_wystawienia']) . '</span></div>';
				echo '<div class="text">'.$r['tresc_komentarzu'].'</div>';
				echo '<div class="actions"><a class="reply" onclick="document.getElementById(\'reply'.$r['id_komentarzu'].'\').style.cssText = \'display: block;\'">Odpowiedz</a></div>';
				echo '<form method="POST" class="ui reply form" id="reply'.$r['id_komentarzu'].'" action="./dodaj_komentarz/'.$id.'/'.$r['id_komentarzu'].'" style="display: none;">';
					echo '<div class="field">';
						echo '<textarea name="opis"></textarea>';
					echo '</div>';
					echo '<button class="ui primary submit labeled icon button" type="submit"><i class="icon edit"></i> Odpowiedz</button>';
				echo '</form>';
				echo '<div class="comments">';
					showCommentsRecursively($id, $r['id_komentarzu']);
				echo '</div>';
			echo '</div>';
		echo '</div>';
	}
}

function deleteAll($str)
{
    if (is_file($str))
		{
        return unlink($str);
    }
    elseif (is_dir($str))
		{
        $scan = glob(rtrim($str,'/').'/*');
        foreach($scan as $index=>$path) {
            deleteAll($path);
        }
    }
}

function showPagination($sites, $current, $adr, $suffix = '') //sites - ilosc stron, current - aktualna, adr - adres
{
	if($sites > 1){
		echo '<div class="one column row centered">';
		echo '<div class="ui pagination menu">';

		$poprzednia = $current -1;
		$nastepna = $current + 1;

		echo '<a href="' . $adr . $poprzednia .'" class="item';
		if($poprzednia < 1)
		{
			echo ' disabled';
		}
		echo '">Poprzednia</a>';

		for($i = 0; $i < $sites; $i++)
		{
			$a = ($i + 1);
			if($a != 1 && abs($current - $a) > 1 && $a != $sites)
			{
				echo '<div class="item">...</div>';
				if($a < $current)
					$i = $current - 3;
				else
					$i = $sites - 2;
				continue;
			}
			echo '<a href="' . $adr . $a . '/' . $suffix . '" class="item';
			if($current == $a) echo ' active';
			echo '">' . $a . '</a>';
		}


		echo '<a href="' . $adr . $nastepna .'" class="item';
		if($nastepna > $sites)
		{
			echo ' disabled';
		}
		echo '">Następna</a>';

		echo '</div>';
		echo '</div>';
	}

}
?>

<?php
session_start();
ob_start();
error_reporting(E_ALL);
require('funkcje.php');
if(!isset($_SESSION['zalogowany'])) $_SESSION['zalogowany'] = 0;

$request = str_replace('/tablica/', '', $_SERVER['REQUEST_URI']);
$params = explode('/', $request);

if($params[0] == 'wyloguj' && $_SESSION['zalogowany'] == 1){
	session_destroy();
	header("Location: /tablica/");
}

$db = new mysqli('localhost', 'tablica', 'tablica123', 'tablica');
$db->set_charset("utf8");

$site = 'wyswietl';
if(!empty($params[0]))
	$site = array_shift($params);
$site_filename = $site . '.php';
if(!is_file($site_filename))
	$site_filename = 'blad.php';

$zalogowany = $_SESSION['zalogowany'] == 1;
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<base href="http://localhost/tablica/">
	<link rel="stylesheet" href="styles/styles.css">
	<link rel="stylesheet" href="styles/stopka.css">
	<link rel="stylesheet" href="semantic/semantic.min.css">
	<link rel="stylesheet" href="styles/galeria.css">
	<script src="jquery-3.3.1.min.js"></script>

	<link href="dropzone/dropzone.min.css" type="text/css" rel="stylesheet" />
	<script src="dropzone/dropzone.min.js"></script>

	<script src="semantic/semantic.min.js"></script>
	<link rel="icon" type="image/ico"  href="images/favicon.ico">
	<title>Tablica ogłoszeń</title>
	<link href="lightgallery/css/lightgallery.css" rel="stylesheet">
	<script src="lightgallery/js/lightgallery.min.js"></script>
	<script src="lightgallery/js/lg-fullscreen.min.js"></script>
	<script src="lightgallery/js/lg-zoom.min.js"></script>
	<script src="lightgallery/js/lg-pager.min.js"></script>

</head>
<body>


	<div class="ui fixed inverted main menu" style="width: 100%;">
		<a href="./" class="item">Strona Główna</a>
		<div class="ui labeled icon dropdown item">
		  	<div class="text">Kategorie</div>
				<i class="dropdown icon"></i>
			  <div class="menu">
					<?php
						$kategoria_q = 'select * from kategorie order by nazwa_kategorii asc;';
						$kat_q = $db->query($kategoria_q);
						foreach($kat_q as $k)
						{
							echo '<a href="./kategoria/' . $k['id_kategoria'] . '" class="item">' . $k['nazwa_kategorii'] . '</a>';
						}
					?>
				</div>

			</div>

			<a href="./archiwum" class="item">Archiwum</a>
			<div class="item">
					<form method="POST" action="./szukaj" class="ui transparent inverted icon input">
						<i class="search icon"> </i>
						<input type="text" name="szukaj" style="width:300px"; placeholder="Wyszukaj ogłoszenie" autocomplete="off">
					</form>
				</div>
		<div class="right menu">
			<?php if($zalogowany) { ?>
			<a href="./dodaj" class="item">Dodaj ogłoszenie&nbsp;&nbsp;<i class="plus square outline icon"></i></a>
		<?php }

		if(!$zalogowany){
		?>
			<a href="./logowanie" class="item">Logowanie</a>
		<?php

		} else {
				$zlicz = $db->query('select count(*) as licznik from wiadomosci where wiadomosc_do ="'.$_SESSION['id_uzytkownika'].'" and przeczytane = "0"');
				$licz = $zlicz->fetch_array(MYSQLI_ASSOC);
				$wynik = $licz['licznik'];
			?>


			<a class="ui labeled icon item" href="./odebrane"><i class="envelope outline icon">&nbsp;(<?php echo $wynik;?>)&nbsp;</i></a>
			<div class="ui labeled icon dropdown item">
		  	<div class="text"><img class="ui mini image" src="<?php echo getUserAvatar($_SESSION['nazwa_uzytkownika']); ?>"> Witaj, <?=$_SESSION['nazwa_uzytkownika'] ?></div>
				<i class="dropdown icon"></i>
			  <div class="menu">
					<?php
						if($_SESSION['zalogowany']==1 && $_SESSION['admin']==1)
							{
									echo '<a href="./adminpanel" class="item">Panel Administracyjny</a>';
							}
					?>
					<a href="./userprofil/<?php echo $_SESSION['id_uzytkownika']; ?>" class="item">Mój profil</a>
					<a href="./mojeogloszenia" class="item">Moje ogłoszenia</a>
					<a href="./obserwowane" class="item">Obserwowane</a>
					<a href="./wyloguj" class="item">Wyloguj</a>
				</div>
			</div>
		<?php } ?>
	</div>
</div>


<div class="ui inverted fixed menu hamburger">
<a class="item" onclick="$('.ui.sidebar').sidebar('toggle');">
	<i class="sidebar icon"></i>
	Menu
</a>
</div>

<div class="ui sidebar inverted vertical accordion menu">
	<div class="item">
			<form method="POST" action="./szukaj" class="ui transparent inverted icon input">
				<i class="search icon"> </i>
				<input type="text" name="szukaj" style="width:300px"; placeholder="Wyszukaj ogłoszenie" autocomplete="off">
			</form>
		</div>
	<a href="./" class="item">Strona Główna</a>
	<div class="item">
			<a class="title">
				<i class="dropdown icon"></i>
				Kategorie
			</a>
			<div class="content">
				<?php
					$kategoria_q = 'select * from kategorie order by nazwa_kategorii asc;';
					$kat_q = $db->query($kategoria_q);
					foreach($kat_q as $k)
					{
						echo '<a href="./kategoria/' . $k['id_kategoria'] . '" class="item">' . $k['nazwa_kategorii'] . '</a>';
					}
				?>
			</div>
		</div>


	<?php if($zalogowany) { ?>
		<a href="./dodaj" class="item">Dodaj ogłoszenie&nbsp;&nbsp;<i class="plus square outline icon"></i></a>
	<?php }

	if(!$zalogowany){
	?>
		<a href="./logowanie" class="item">Logowanie</a>
	<?php

	} else {
			$zlicz = $db->query('select count(*) as licznik from wiadomosci where wiadomosc_do ="'.$_SESSION['id_uzytkownika'].'" and przeczytane = "0"');
			$licz = $zlicz->fetch_array(MYSQLI_ASSOC);
			$wynik = $licz['licznik'];
		?>


		<a class="ui labeled icon item" href="./odebrane"><i class="envelope outline icon"></i>Wiadomości (<?php echo $wynik;?>)</a>
		<div class="item">
			<a class="title">
				<i class="dropdown icon"></i>
				<img class="ui mini image" src="<?php echo getUserAvatar($_SESSION['nazwa_uzytkownika']); ?>" style="display: inline; margin-right: 5px;"> Witaj, <?=$_SESSION['nazwa_uzytkownika'] ?>
			</a>

			<div class="content">
				<?php
					if($_SESSION['zalogowany']==1 && $_SESSION['admin']==1)
						{
								echo '<a href="./adminpanel" class="item">Panel Administracyjny</a>';
						}
				?>
				<a href="./userprofil/<?php echo $_SESSION['id_uzytkownika']; ?>" class="item">Mój profil</a>
				<a href="./mojeogloszenia" class="item">Moje ogłoszenia</a>
				<a href="./obserwowane" class="item">Obserwowane</a>
				<a href="./wyloguj" class="item">Wyloguj</a>
			</div>

		</div>
	<?php } ?>
</div>
	<div class="ui container" style="margin-top: 100px; flex: 1;">

		<?php include $site_filename; ?>
	</div>
	<div class="ui inverted vertical footer segment" style="margin-top: 100px; padding: 50px;" id="stopka">
		<div class="ui container">
			<div class="ui stackable inverted divided equal height stackable grid centered">
				Copyright © 2019 KKK
			</div>
		</div>
	</div>

	<script>
		$('.ui.dropdown').dropdown();

		$('.ui.accordion').accordion();


    $(document).ready(function() {
        $(".galeria").lightGallery();
    });
	</script>
</body>
</html>

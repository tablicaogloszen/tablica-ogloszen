<form method="post" class="ui form">
  <div class="inline fields">
      <div class="eight wide field">
        <input type="text" name="fraza" placeholder="Fraza" />
      </div>
      <div class="field">
        <button type="submit" class="ui green labeled icon button">
          <i class="add icon"></i>
          Dodaj
        </button>
      </div>
  </div>
</form>

<?php
if(!empty($params[0]))
{
  $db->query('delete from obserwowane where id=' . $params[0]);
  header("Location: /tablica/obserwowane/");
}
echo '<h1 style="ui header">Obserwowane</h1>';
if(isset($_POST['fraza']))
{
  $db->query('insert into obserwowane values(null, "'.$_POST['fraza'].'", '.$_SESSION['id_uzytkownika'].')');
  header("Location: /tablica/obserwowane/");
}
$query_obserwowane = 'select * from obserwowane where id_uzytkownika=' . $_SESSION['id_uzytkownika'];
$obserwowane = $db->query($query_obserwowane);
if($obserwowane->num_rows > 0)
{
  echo '<table class="ui table">';
  foreach ($obserwowane as $o)
  {
	  $zliczobs = $db->query('select count(*) as ilosc from ogloszenie join users on ogloszenie.fk_id_user=users.id_user join kategorie on ogloszenie.fk_id_kategoria=kategorie.id_kategoria join wojewodztwa on ogloszenie.fk_id_wojewodztwa=wojewodztwa.id_wojewodztwa where ogloszenie.nazwa LIKE "%'.$o['fraza'].'%" order by ogloszenie.data_wystawienia desc, ogloszenie.id_ogloszenie desc');
				$liczobs = $zliczobs->fetch_array(MYSQLI_ASSOC);
				$wyniczek = $liczobs['ilosc'];
    echo '<tr>';
    echo '<td><a href="obserwowanewyswietl/'.$o['fraza'].'/">' . $o['fraza'] . '</a></td><td>Ilość ogłoszeń o podanej frazie: <b>'.$wyniczek.'</b></td><td class="right aligned"><a class="ui red labeled icon button" href="obserwowane/' . $o['id'] . '"><i class="trash icon"></i>usuń</a></td>';
    echo '</tr>';
  }
  echo '</table>';
}
?>


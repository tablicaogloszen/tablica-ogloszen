<?php
$query = 'select ogloszenie.*, users.username, users.email, kategorie.id_kategoria, kategorie.nazwa_kategorii, wojewodztwa.nazwa_wojewodztwa from ogloszenie join users on ogloszenie.fk_id_user = users.id_user join kategorie on ogloszenie.fk_id_kategoria=kategorie.id_kategoria join wojewodztwa on ogloszenie.fk_id_wojewodztwa=wojewodztwa.id_wojewodztwa where id_ogloszenie="' . $params[0] . '" limit 1;';
$action = 'szczegoly';
$q = $db->query($query);
if($q->num_rows > 0){
	$q_array = $q->fetch_array(MYSQLI_ASSOC);
	$nazwa = $q_array['nazwa'];
	$opis = $q_array['opis'];
	$cena = $q_array['cena'];
	$id_kategorii = $q_array['id_kategoria'];
	$nazwa_kategorii = $q_array['nazwa_kategorii'];
	$data_wystawienia = $q_array['data_wystawienia'];
	$uzytkownik = $q_array['username'];
	$email = $q_array['email'];
	$id = $q_array['id_ogloszenie'];
	$miasto = $q_array['miasto'];
	$wojewodztwo = $q_array['nazwa_wojewodztwa'];
	$dataPolska = convertDate($data_wystawienia);
	$userid = $q_array['fk_id_user'];
?>
<article class="ui piled segment">
	<div class="ui two column stackable grid">
	<div class="twelve wide column">

	<div style="float:left;">
	<h1 class="ui dividing header"><?php echo wordwrap($nazwa, 60, '<br />', true) ?>
		</div>
		<div style="float:right;">
		<?php
		if(($zalogowany && $userid == $_SESSION['id_uzytkownika'])|| ($zalogowany && $_SESSION['admin'] == 1)){
			echo '<a href="edytuj/' . $id . '" class="ui icon button right floated"><i class="pencil icon"></i></a>';
		}
		?>
		</div>
		</h1>
		<div style="clear:both;"></div>

		<div style="margin-top: 10px;">

		<p><?php echo $opis; ?></p>

		<?php
		if(isArticleImage($id))
		{
			$directory = 'images/ogloszenia/' . $id . '/';
			//$images = scandir($directory);
			$images = array_diff(scandir($directory), array('.', '..', 'thumbnail'));
		echo '<div class="ui bordered images galeria">';
			foreach($images as $image)
			{
				//echo '<a href="' . $directory . $image . '">';
				echo '<img data-src="'.$directory.$image.'" src="' . $directory;
				if(is_file($directory . 'thumbnail/' . $image)) echo 'thumbnail/';
				echo $image . '"/>';
				//echo '</a>';
			}
		echo '</div>';
			}
		?>
</div>
	</div>
	<div class="four wide column">
		<div class="ui massive tag labels">
			<div class="ui yellow label">
			<?php
			if($cena == '0')
				echo 'Za darmo';
			else
				echo $cena . 'zł';
			?>
			</div>
		</div>


		<h3 class="ui header">Lokalizacja:<div class="sub header"><?php echo $miasto; ?> (<?php echo $wojewodztwo;?>)</div></h3>
		<h3 class="ui header">Kategoria:<div class="sub header"><a href="./kategoria/<?php echo $id_kategorii; ?>"><?php echo $nazwa_kategorii; ?></a></div></h3>
		<h3 class="ui header">Użytkownik:<div class="sub header"><?php echo ' <a href="./userprofil/'.$userid.'">' . $uzytkownik . ' (<a href="mailto:' . $email . '">' . $email . '</a>)</div></h3>';?>
		<h3 class="ui header">Dodano:<div class="sub header"><?php echo $dataPolska; ?></div></h3>

		<h3>
		<div class="ui center floated buttons">
				<a href="./zapytajoogloszenie/<?php echo $id;?>" class="positive ui button"><i class="pencil icon"></i> Zapytaj o przedmiot</a>
			</div>
		</h3>

</div>
</div>
</article>

<script>
function myFunction(imgs) {
  var expandImg = document.getElementById("expandedImg");
  var imgText = document.getElementById("imgtext");
  expandImg.src = imgs.src;
  imgText.innerHTML = imgs.alt;
  expandImg.parentElement.style.display = "block";
}
</script>
<?php
}
else
{
	$query = 'select count(*) as c from archiwum where id_ogloszenie="' . $params[0] . '" limit 1;';
	$q = $db->query($query)->fetch_array(MYSQLI_ASSOC);
	if($q['c'] > 0)
	{
		header("Location: /tablica/archiwum/" . $params[0]);
	}

	echo '<h1 class="ui header">Nie znaleziono ogłoszenia</h1>';

}

?>

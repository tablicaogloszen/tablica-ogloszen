
<?php
if(isset($_POST['szukaj']))
{
	header("Location: /tablica/szukaj/" . $_POST['szukaj']);
}
$wyszukiwane = array_shift($params);
$strona = !empty($params[0]) && is_numeric($params[0]) ? array_shift($params) : 1;
$ilosc_na_strone = 6;
$limit = 'limit ' . (($strona - 1) * $ilosc_na_strone) . ', ' . ($ilosc_na_strone);
$ogloszenia = $db->query('select * from ogloszenie join users on ogloszenie.fk_id_user=users.id_user join kategorie on ogloszenie.fk_id_kategoria=kategorie.id_kategoria join wojewodztwa on ogloszenie.fk_id_wojewodztwa=wojewodztwa.id_wojewodztwa where ogloszenie.nazwa like "%'.$wyszukiwane.'%"  order by ogloszenie.data_wystawienia desc, ogloszenie.id_ogloszenie desc ' . $limit);
$licznik = $db->query('select count(*) as wynik from ogloszenie join users on ogloszenie.fk_id_user=users.id_user join kategorie on ogloszenie.fk_id_kategoria=kategorie.id_kategoria join wojewodztwa on ogloszenie.fk_id_wojewodztwa=wojewodztwa.id_wojewodztwa where ogloszenie.nazwa like "%'.$wyszukiwane.'%"  order by ogloszenie.data_wystawienia desc, ogloszenie.id_ogloszenie desc');
$li = $licznik->fetch_array(MYSQLI_ASSOC);
$wynik = $li['wynik'];
$ilosc_stron = ceil($wynik / $ilosc_na_strone);
if($wynik!=0)
{
	echo '<h1 class="ui header">Wyszukiwana fraza: '.$wyszukiwane.'</h1>';
	echo '<div class="ui two column stackable grid">';
	include 'ogloszenia.php';
	showPagination($ilosc_stron, $strona, './szukaj/' . $wyszukiwane . '/');
	echo'</div>';
}
elseif($wynik==0)
{
	echo '<h1 class="ui header">Brak ogłoszeń o wyszukiwanej frazie</h1>';
}
?>

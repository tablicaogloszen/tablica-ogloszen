-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 24 Maj 2019, 17:50
-- Wersja serwera: 10.1.38-MariaDB
-- Wersja PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `tablica`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `archiwum`
--

CREATE TABLE `archiwum` (
  `id_ogloszenie` int(11) NOT NULL,
  `nazwa` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `opis` text COLLATE utf8_polish_ci,
  `cena` float DEFAULT NULL,
  `data_wystawienia` datetime DEFAULT NULL,
  `fk_id_kategoria` int(10) NOT NULL,
  `fk_id_user` int(10) NOT NULL,
  `miasto` varchar(255) COLLATE utf8_polish_ci NOT NULL DEFAULT '-',
  `fk_id_wojewodztwa` int(11) DEFAULT '17'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `archiwum`
--

INSERT INTO `archiwum` (`id_ogloszenie`, `nazwa`, `opis`, `cena`, `data_wystawienia`, `fk_id_kategoria`, `fk_id_user`, `miasto`, `fk_id_wojewodztwa`) VALUES
(1, 'Kotki', 'Oddam kotki chodowane w piwnicy w ilości 3. Nie wykazują zdolności do przemocy. Polecam!', 20, '2019-02-28 13:00:00', 7, 1, '-', 17),
(2, 'Psy', 'hau hau', 100, '2019-02-28 13:05:00', 7, 1, '-', 17),
(3, 'Laptop Dell', 'Sprawny, nie działa', 2500, '2019-02-28 13:50:00', 7, 1, '-', 17),
(61, 'dfsdfasdfsdf', 'sdf gdfg sdfgdsf g dg sdf', 2342340, '2019-04-24 14:14:32', 1, 1, 'Dfsgdgdfg', 13);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `kategorie`
--

CREATE TABLE `kategorie` (
  `id_kategoria` int(11) NOT NULL,
  `nazwa_kategorii` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `kategorie`
--

INSERT INTO `kategorie` (`id_kategoria`, `nazwa_kategorii`) VALUES
(1, 'Elektronika'),
(2, 'Motoryzacja'),
(3, 'AGD'),
(4, 'Antyki'),
(5, 'Chemia'),
(6, 'Spożywcze'),
(7, 'Inne');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `komentarze`
--

CREATE TABLE `komentarze` (
  `id_komentarzu` int(11) NOT NULL,
  `id_nadrzednego` int(11) DEFAULT NULL,
  `data_wystawienia` datetime DEFAULT NULL,
  `dla_kogo` int(11) DEFAULT NULL,
  `kto_dal` int(11) DEFAULT NULL,
  `tresc_komentarzu` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `komentarze`
--

INSERT INTO `komentarze` (`id_komentarzu`, `id_nadrzednego`, `data_wystawienia`, `dla_kogo`, `kto_dal`, `tresc_komentarzu`) VALUES
(1, NULL, '2019-03-28 06:18:00', 1, 20, 'Testowy komentarz'),
(6, NULL, '2019-03-28 06:18:00', 1, 20, '123'),
(7, NULL, '2019-03-28 06:18:00', 1, 20, '345'),
(8, NULL, '2019-03-28 20:47:58', 29, 1, 'adsad'),
(9, NULL, '2019-03-28 20:48:23', 1, 1, 'wszytsko sprawnie, polecam'),
(10, NULL, '2019-03-28 20:57:37', 0, 1, 'TEST 1234'),
(22, NULL, '2019-03-28 21:05:46', 0, 1, 'testowy'),
(23, NULL, '2019-03-29 11:49:52', 0, 1, '123'),
(24, NULL, '2019-03-29 12:09:25', 0, 1, 'adkapldkpa'),
(25, 8, '2019-04-08 22:34:26', 29, 1, 'sdf fdgsdfg dsfg sdf g'),
(26, NULL, '2019-04-08 22:58:08', 29, 1, 'Super, gut, alles klar! :)'),
(27, NULL, '2019-04-08 22:58:27', 29, 1, '6 sterne, polecam very much'),
(28, 25, '2019-04-08 22:58:54', 29, 1, 'Nie byłbym taki pewny, ja się przejechałem, nie polecam.'),
(29, 26, '2019-04-08 23:05:14', 29, 1, 'Das ist odpowiedź na alles klar'),
(30, 1, '2019-04-08 23:08:45', 1, 1, 'Testowa odpowiedź'),
(31, NULL, '2019-04-08 23:09:34', 1, 1, '567'),
(32, NULL, '2019-04-08 23:24:04', 1, 1, 'Nowy komentarz'),
(33, 26, '2019-04-08 23:24:44', 29, 1, 'ja, alles git'),
(34, 26, '2019-04-08 23:26:03', 29, 29, 'Danke');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `obserwowane`
--

CREATE TABLE `obserwowane` (
  `id` int(11) NOT NULL,
  `fraza` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `id_uzytkownika` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `obserwowane`
--

INSERT INTO `obserwowane` (`id`, `fraza`, `id_uzytkownika`) VALUES
(2, 'auto', 29),
(3, 'koń', 29),
(4, 'komputer', 29),
(5, 'auto', 1),
(6, 'komputer', 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `oceny`
--

CREATE TABLE `oceny` (
  `id_oceny` int(11) NOT NULL,
  `dla_kogo` int(11) DEFAULT NULL,
  `od_kogo` int(11) DEFAULT NULL,
  `ocena` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `oceny`
--

INSERT INTO `oceny` (`id_oceny`, `dla_kogo`, `od_kogo`, `ocena`) VALUES
(33, 27, 1, -1),
(35, 1, 29, -1),
(36, 29, 29, 1),
(38, 20, 20, 1),
(39, 28, 1, 1),
(48, 29, 1, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ogloszenie`
--

CREATE TABLE `ogloszenie` (
  `id_ogloszenie` int(11) NOT NULL,
  `nazwa` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `opis` text COLLATE utf8_polish_ci,
  `cena` float DEFAULT NULL,
  `data_wystawienia` datetime DEFAULT NULL,
  `fk_id_kategoria` int(10) NOT NULL,
  `fk_id_user` int(10) NOT NULL,
  `miasto` varchar(255) COLLATE utf8_polish_ci NOT NULL DEFAULT '-',
  `fk_id_wojewodztwa` int(11) DEFAULT '17'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `ogloszenie`
--

INSERT INTO `ogloszenie` (`id_ogloszenie`, `nazwa`, `opis`, `cena`, `data_wystawienia`, `fk_id_kategoria`, `fk_id_user`, `miasto`, `fk_id_wojewodztwa`) VALUES
(11, 'Passat', 'Niemiec płakał jak sprzedawał', 12342, '2019-03-01 00:00:00', 2, 1, '-', 17),
(12, 'Chomik', 'Syberyjski', 30, '2019-03-01 00:00:00', 7, 1, '-', 14),
(13, 'Szafa', 'Drewniana, posiada metalowe gałki', 130, '2019-03-01 00:00:00', 7, 1, '-', 17),
(14, 'Audi', 'Nowka sztuka', 12000, '2019-03-01 17:37:28', 2, 1, '-', 17),
(15, 'Rybka', 'Złota rybka', 11, '2019-03-01 17:39:10', 7, 1, '-', 17),
(16, 'Nissan', 'xD', 12344, '2019-03-03 12:52:19', 2, 1, '-', 17),
(17, 'Marek', 'asjidoasjdsoi', 0, '2019-03-04 13:26:45', 7, 1, '-', 17),
(18, 'Laptop Acer', 'Używany', 1300, '2019-03-06 17:11:38', 7, 1, '-', 17),
(19, 'Seat', 'Nowka sztuka', 12000, '2019-03-01 17:37:28', 2, 1, '-', 17),
(20, 'VW', 'Nowka sztuka', 12000, '2019-03-01 17:37:28', 2, 1, '-', 17),
(21, 'Panda', 'Nowka sztuka', 12000, '2019-03-01 17:37:28', 2, 1, '-', 17),
(22, 'Fiat', 'Nowka sztuka', 12000, '2019-03-01 17:37:28', 2, 1, '-', 17),
(23, 'Saab', 'Nowka sztuka', 12000, '2019-03-01 17:37:28', 2, 1, '-', 17),
(24, 'Rybka', 'Złota', 12344, '2019-03-10 21:52:54', 7, 1, '-', 17),
(25, 'Komputer', 'oddam za czekolade', 0, '2019-03-10 22:21:42', 7, 1, '-', 17),
(26, 'Kubek', 'Termiczny srebrny', 12, '2019-03-10 22:22:03', 7, 1, '-', 17),
(27, 'Poduszka ', 'naturalna z pierzem', 120, '2019-03-10 22:22:18', 7, 1, '-', 17),
(28, 'Gitara', 'Elektryczna sprawna', 654, '2019-03-10 22:22:40', 7, 1, '-', 17),
(29, '4move', 'Cytrynowy', 3, '2019-03-11 12:19:18', 6, 20, '-', 17),
(30, 'Zestaw perfum', 'Perfumy marki Nike - idealny zapach ', 130, '2019-03-13 12:17:35', 7, 20, '-', 17),
(31, '4move', 'Babanowy', 3, '2019-03-11 12:19:18', 7, 20, '-', 17),
(32, 'Zestaw perfum', 'Perfumy marki Playboy - idealny zapach ', 130, '2019-03-13 12:17:35', 7, 20, '-', 17),
(33, 'Zestaw perfum', 'Perfumy marki Jop - idealny zapach ', 100, '2019-03-13 12:17:35', 7, 20, '-', 17),
(35, 'Dzbanek', 'Metalowy', 57, '2019-03-13 12:17:35', 3, 1, '-', 17),
(36, 'Nissan', 'ldapsd', 12344, '2019-03-22 20:45:22', 1, 1, '-', 17),
(37, 'Nissan', 'Oddam', 1344, '2019-03-22 20:46:03', 1, 1, '-', 17),
(38, 'Audica', 'asdjoads', 12344, '2019-03-22 21:23:30', 2, 1, 'Opole', 8),
(39, 'Rybka', 'ad', 12, '2019-03-22 21:25:25', 7, 1, 'Wrocław', 17),
(40, 'Nissan', 'asdasd', 123000, '2019-03-22 21:26:30', 1, 1, 'Warszawa', 17),
(41, 'fifa 20', 'uzywana, brak widocznych sladow', 1000, '2019-03-22 21:26:58', 7, 1, 'Gdańsk', 17),
(42, 'Renault Clio', 'Szare, opony gratis', 4000, '2019-03-22 21:29:02', 2, 1, 'Opole Wschód', 17),
(43, 'Pralka', 'pralka frania - do oddania', 0, '2019-03-22 21:44:33', 3, 1, 'Kotórz mały', 8),
(44, 'Zmywarka Bosh', 'Sama zmywa', 1333, '2019-03-24 10:59:53', 3, 1, 'Piotrówka', 15),
(45, 'Dell Podstawka', 'podstawka pod laptopa', 0, '2019-03-24 11:01:05', 0, 1, 'Opole', 17),
(46, 'Kubek', 'Zmienia kolor jak jest ciepły', 20, '2019-03-24 14:49:24', 7, 1, 'Piotrówka', 15),
(47, 'Nissan', 'za friko', 0, '2019-03-24 15:10:26', 2, 1, 'Wrocław', 5),
(48, 'VW ', 'za friko', 3000, '2019-03-24 15:10:57', 2, 1, 'Piotrówka', 15),
(49, 'Rybka', 'Złota rybka', 21, '2019-03-24 15:15:02', 7, 1, 'Łódź', 5),
(50, 'Passat', 'rdzewieje', 1333, '2019-03-24 15:18:41', 2, 1, 'Kraków', 7),
(51, 'Cheetos Serowe', 'Serowe chrupki kultowej firmy cheetos', 3, '2019-03-26 18:00:08', 6, 1, 'Kraków', 6),
(52, 'Telefon Xiaomi Redmi 5A', 'Używany przez o. Mateusza', 350, '2019-03-27 16:34:40', 1, 29, 'Sandomierz', 13),
(54, 'auto', 'Na chodzie, papiery ok', 100, '2019-04-02 20:42:42', 2, 29, 'Katowice', 12),
(55, 'auto', 'Na chodzie, papiery ok', 100, '2019-04-02 20:42:58', 2, 29, 'Katowice', 12),
(56, 'małe auto', 'dfgdfg', 10, '2019-04-02 20:43:59', 1, 29, 'Kotórz Mały', 13),
(57, 'małe auto', 'dfgdfg', 10, '2019-04-02 20:44:32', 1, 1, 'Kotórz Mały', 13),
(58, 'małe auto', 'dfgdfg', 10, '2019-04-02 20:44:48', 1, 29, 'Kotórz Mały', 13),
(59, 'komputer', 'Tanio, Pentium III na slot', 0, '2019-04-02 21:04:26', 1, 1, 'Świnoujście', 11),
(60, 'Komputer Hi-Tech2', 'Zdjęcie dla zachęty.\r\n\r\nPentium III 233MHz, 32MB SDRAM, VIA 2MB', 1500, '2019-04-24 14:11:18', 1, 1, 'Świnoujście', 16),
(62, 'auto', 'Sprawne, nie działa.', 150000, '2019-04-24 14:16:06', 2, 1, 'Tarnowskie Góry', 12),
(63, 'testowa', 'erwwerwre', 13, '2019-04-24 16:09:32', 10, 1, 'Asd', 12),
(64, 'Kosek 123', 'Przykładowy tekst', 123, '2019-04-24 16:13:43', 1, 1, 'Opole', 13),
(66, 'allalalalalalla alalaas ble ble Laptop', 'Ogłoszenie bla bla bla', 0, '2019-05-02 10:30:10', 1, 1, 'Wróblin', 13);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `username` varchar(30) COLLATE utf8_polish_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `admin` tinyint(1) NOT NULL,
  `avatar_name` varchar(255) COLLATE utf8_polish_ci DEFAULT 'default.png'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`id_user`, `username`, `email`, `password`, `admin`, `avatar_name`) VALUES
(1, 'admin', 'admin@admin', '21232f297a57a5a743894a0e4a801fc3', 1, 'default.png'),
(20, 'Kosek', 'kosektomasz96@gmail.com', 'b849db7f2c7b1439fcfcaa47c6c0f520', 1, 'barca.png'),
(22, 'Test', 'test@wp.pl', '098f6bcd4621d373cade4e832627b4f6', 0, 'default.png'),
(23, 'Sanderka', 'sandeka@sandra.pl', '16d7a4fca7442dda3ad93c9a726597e4', 0, 'default.png'),
(24, 'simple', 'simple@simple.com', '202cb962ac59075b964b07152d234b70', 0, 'default.png'),
(25, 'szkola', 'szkola@wp.pl', 'be8287c6f4dce7ff1c93c8d94b3a1032', 0, 'default.png'),
(27, 'Adminek', 'admin@adminek.pl', '202cb962ac59075b964b07152d234b70', 0, 'default.png'),
(29, 'krystian', 'krystian.komor@gmail.com', 'c75b6bdde1d90eb183d6fad61bb8da56', 1, 'default.png'),
(30, 'Ping', 'ping@asd.pl', '202cb962ac59075b964b07152d234b70', 0, 'default.png');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wiadomosci`
--

CREATE TABLE `wiadomosci` (
  `id_wiadomosci` int(11) NOT NULL,
  `tresc_wiadomosci` text COLLATE utf8_polish_ci,
  `wiadomosc_od` int(11) DEFAULT NULL,
  `wiadomosc_do` int(11) DEFAULT NULL,
  `przeczytane` tinyint(1) DEFAULT NULL,
  `data_wiadomosci` datetime DEFAULT NULL,
  `temat` varchar(100) COLLATE utf8_polish_ci DEFAULT NULL,
  `czyja_wiadomosc` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `wiadomosci`
--

INSERT INTO `wiadomosci` (`id_wiadomosci`, `tresc_wiadomosci`, `wiadomosc_od`, `wiadomosc_do`, `przeczytane`, `data_wiadomosci`, `temat`, `czyja_wiadomosc`) VALUES
(33, 'hi', 1, 21, 1, '2019-04-02 16:49:07', '', 1),
(34, 'test', 1, 1, 1, '2019-04-02 16:51:00', '', 1),
(35, 'hi', 1, 21, 1, '2019-04-02 16:54:48', '', 1),
(36, 'siemka', 1, 30, 1, '2019-04-02 16:56:39', '', 1),
(37, 'hi', 21, 1, 1, '2019-04-02 16:59:59', '', 1),
(38, 'co tam?', 1, 21, 1, '2019-04-02 17:07:43', '', 1),
(39, 'wiadomosc', 1, 30, 1, '2019-04-02 17:11:28', '', 1),
(40, 'hihihi', 1, 30, 1, '2019-04-02 17:12:01', '', 1),
(41, 'siema', 30, 1, 1, '2019-04-02 17:21:41', '', 1),
(42, 'elo', 30, 30, 1, '2019-04-02 17:23:06', '', 1),
(43, 'elo', 30, 1, 1, '2019-04-02 17:23:56', '', 1),
(44, 'siema', 1, 20, 1, '2019-04-02 17:33:46', '', 1),
(45, 'test', 20, 30, 1, '2019-04-02 17:38:13', '', 1),
(46, 'Znaleziono oberwowane ogłoszenie. <a href=\"wyswietl/58\">małe auto</a>', 1, 29, 1, '2019-04-02 20:44:48', 'Obserwowane', 1),
(47, 'Znaleziono oberwowane ogłoszenie. <a href=\"szczegoly/56\">małe auto</a>', 1, 29, 1, '2019-04-02 20:45:53', 'Obserwowane', 1),
(48, 'Znaleziono oberwowane ogłoszenie. <a href=\"szczegoly/59\">komputer</a>', 1, 29, 1, '2019-04-02 21:04:26', 'Obserwowane', 1),
(49, 'Dodano kowy komentarz do Twojego <a href=\"./userprofil/1\">profilu.</a>', 1, 1, 1, '2019-04-08 23:24:04', 'Nowy komentarz', 1),
(50, 'Dodano kowy komentarz do Twojego <a href=\"./userprofil/29\">profilu.</a>', 29, 29, 1, '2019-04-08 23:24:44', 'Nowy komentarz', 1),
(51, 'Dodano kowy komentarz do Twojego <a href=\"./userprofil/29\">profilu.</a>', 29, 29, 1, '2019-04-08 23:26:03', 'Nowy komentarz', 1),
(52, 'Odpisuję.', 1, 29, 1, '2019-04-09 00:04:18', '', 1),
(53, 'super. cieszę się bardzo', 29, 1, 1, '2019-04-09 00:15:19', '', 1),
(54, 'bla bla bla', 29, 1, 1, '2019-04-09 00:38:20', NULL, 1),
(55, 'działa?', 29, 1, 1, '2019-04-09 00:38:36', NULL, 1),
(56, 'owszem, bez problemów\r\ni bez zarzutów\r\n:)', 1, 29, 1, '2019-04-09 00:39:02', NULL, 1),
(57, '', 1, 1, 1, '2019-04-22 09:07:30', NULL, 1),
(63, 'Czy ogloszenie http://localhost/tablica/szczegoly/59/ jest nadal dostępne?', 1, 29, 1, '2019-04-22 15:48:44', '', 1),
(64, 'Czy ogloszenie http://localhost/tablica/szczegoly/59/ jest nadal dostępne?', 1, 29, 1, '2019-04-22 15:50:10', '', 1),
(65, 'Czy ogloszenie http://localhost/tablica/szczegoly/38/ jest nadal dostępne?', 1, 1, 1, '2019-04-22 15:50:25', '', 1),
(66, '', 1, 29, 1, '2019-04-22 16:13:59', NULL, 1),
(67, 'Znaleziono oberwowane ogłoszenie. <a href=\"szczegoly/59\">komputer</a>', 1, 29, 1, '2019-04-24 12:40:10', 'Obserwowane', 1),
(68, 'Znaleziono oberwowane ogłoszenie. <a href=\"szczegoly/60\">Komputer Hi-Tech2</a>', 1, 29, 1, '2019-04-24 14:11:18', 'Obserwowane', 1),
(69, 'Znaleziono oberwowane ogłoszenie. <a href=\"szczegoly/62\">auto</a>', 1, 29, 1, '2019-04-24 14:16:06', 'Obserwowane', 1),
(70, 'Znaleziono oberwowane ogłoszenie. <a href=\"szczegoly/57\">małe auto</a>', 1, 29, 1, '2019-04-24 14:20:15', 'Obserwowane', 1),
(71, 'Znaleziono oberwowane ogłoszenie. <a href=\"szczegoly/65\">auto</a>', 1, 29, 1, '2019-04-24 16:20:26', 'Obserwowane', 1),
(72, 'Znaleziono oberwowane ogłoszenie. <a href=\"szczegoly/65\">auto</a>', 1, 1, 1, '2019-04-24 16:20:26', 'Obserwowane', 1),
(73, 'Czy ogloszenie http://localhost/tablica/szczegoly/65/ jest nadal dostępne?', 1, 1, 1, '2019-04-24 16:25:19', '', 1),
(74, 'Znaleziono oberwowane ogłoszenie. <a href=\"szczegoly/65\">auto</a>', 1, 29, 1, '2019-05-02 10:29:01', 'Obserwowane', 1),
(75, 'Znaleziono oberwowane ogłoszenie. <a href=\"szczegoly/65\">auto</a>', 1, 1, 1, '2019-05-02 10:29:01', 'Obserwowane', 1),
(76, 'Znaleziono oberwowane ogłoszenie. <a href=\"szczegoly/65\">auto</a>', 1, 29, 0, '2019-05-15 12:13:37', 'Obserwowane', 1),
(77, 'Znaleziono oberwowane ogłoszenie. <a href=\"szczegoly/65\">auto</a>', 1, 1, 1, '2019-05-15 12:13:37', 'Obserwowane', 1),
(78, 'Znaleziono oberwowane ogłoszenie. <a href=\"szczegoly/65\">auto</a>', 1, 29, 0, '2019-05-15 13:39:19', 'Obserwowane', 1),
(79, 'Znaleziono oberwowane ogłoszenie. <a href=\"szczegoly/65\">auto</a>', 1, 1, 1, '2019-05-15 13:39:19', 'Obserwowane', 1),
(80, 'Znaleziono oberwowane ogłoszenie. <a href=\"szczegoly/65\">auto</a>', 1, 29, 0, '2019-05-15 13:39:27', 'Obserwowane', 1),
(81, 'Znaleziono oberwowane ogłoszenie. <a href=\"szczegoly/65\">auto</a>', 1, 1, 1, '2019-05-15 13:39:27', 'Obserwowane', 1),
(82, '', 1, 1, 1, '2019-05-24 13:21:42', NULL, 1),
(83, '', 1, 1, 1, '2019-05-24 13:27:30', NULL, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wojewodztwa`
--

CREATE TABLE `wojewodztwa` (
  `id_wojewodztwa` int(10) NOT NULL,
  `nazwa_wojewodztwa` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `wojewodztwa`
--

INSERT INTO `wojewodztwa` (`id_wojewodztwa`, `nazwa_wojewodztwa`) VALUES
(1, 'dolnośląskie'),
(2, 'kujawsko-pomorskie'),
(3, 'lubelskie'),
(4, 'lubuskie'),
(5, 'łódzkie'),
(6, 'małopolskie'),
(7, 'mazowieckie'),
(8, 'opolskie'),
(9, 'podkarpackie'),
(10, 'podlaskie'),
(11, 'pomorskie'),
(12, 'śląskie'),
(13, 'świętokrzyskie'),
(14, 'warmińsko-mazurskie'),
(15, 'wielkopolskie'),
(16, 'zachodniopomorskie'),
(17, 'nie podano');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `zgloszenia`
--

CREATE TABLE `zgloszenia` (
  `id_zgloszenia` int(11) NOT NULL,
  `zglaszajacy` int(11) DEFAULT NULL,
  `zgloszony` int(11) DEFAULT NULL,
  `powod` text CHARACTER SET latin1,
  `czy_zalatwione` int(11) DEFAULT '0',
  `fk_id_admina` int(11) DEFAULT '0',
  `data_wykonania` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `zgloszenia`
--

INSERT INTO `zgloszenia` (`id_zgloszenia`, `zglaszajacy`, `zgloszony`, `powod`, `czy_zalatwione`, `fk_id_admina`, `data_wykonania`) VALUES
(1, 20, 22, 'Awatar nieodpowiedni', 1, 1, '2019-05-24 17:30:33'),
(2, 1, 22, 'Avatar', 1, 1, '2019-05-24 17:43:19'),
(5, 1, 29, 'Obrazliwy login uzytkownika', 1, 1, '0000-00-00 00:00:00'),
(6, 1, 20, 'Spamowanie ogloszeniami', 0, 0, '0000-00-00 00:00:00'),
(7, 1, 29, 'Oszust', 1, 1, '2019-05-24 17:43:00'),
(8, 29, 20, 'Obrazliwy login uzytkownika', 1, 1, '2019-05-24 17:43:05');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `archiwum`
--
ALTER TABLE `archiwum`
  ADD PRIMARY KEY (`id_ogloszenie`);

--
-- Indeksy dla tabeli `kategorie`
--
ALTER TABLE `kategorie`
  ADD PRIMARY KEY (`id_kategoria`);

--
-- Indeksy dla tabeli `komentarze`
--
ALTER TABLE `komentarze`
  ADD PRIMARY KEY (`id_komentarzu`);

--
-- Indeksy dla tabeli `obserwowane`
--
ALTER TABLE `obserwowane`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `oceny`
--
ALTER TABLE `oceny`
  ADD PRIMARY KEY (`id_oceny`);

--
-- Indeksy dla tabeli `ogloszenie`
--
ALTER TABLE `ogloszenie`
  ADD PRIMARY KEY (`id_ogloszenie`);

--
-- Indeksy dla tabeli `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`);

--
-- Indeksy dla tabeli `wiadomosci`
--
ALTER TABLE `wiadomosci`
  ADD PRIMARY KEY (`id_wiadomosci`);

--
-- Indeksy dla tabeli `wojewodztwa`
--
ALTER TABLE `wojewodztwa`
  ADD PRIMARY KEY (`id_wojewodztwa`);

--
-- Indeksy dla tabeli `zgloszenia`
--
ALTER TABLE `zgloszenia`
  ADD PRIMARY KEY (`id_zgloszenia`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `kategorie`
--
ALTER TABLE `kategorie`
  MODIFY `id_kategoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT dla tabeli `komentarze`
--
ALTER TABLE `komentarze`
  MODIFY `id_komentarzu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT dla tabeli `obserwowane`
--
ALTER TABLE `obserwowane`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT dla tabeli `oceny`
--
ALTER TABLE `oceny`
  MODIFY `id_oceny` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT dla tabeli `ogloszenie`
--
ALTER TABLE `ogloszenie`
  MODIFY `id_ogloszenie` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT dla tabeli `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT dla tabeli `wiadomosci`
--
ALTER TABLE `wiadomosci`
  MODIFY `id_wiadomosci` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;

--
-- AUTO_INCREMENT dla tabeli `wojewodztwa`
--
ALTER TABLE `wojewodztwa`
  MODIFY `id_wojewodztwa` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT dla tabeli `zgloszenia`
--
ALTER TABLE `zgloszenia`
  MODIFY `id_zgloszenia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

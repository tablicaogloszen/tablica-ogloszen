<?php
echo '<h2 style="ui header">Profil użytkownika</h2>';
	?>

<?php
$query = 'select * from users where id_user="' . $params[0] . '" limit 1;';
$action = 'userprofil';
$query_zlicz = 'select count(*) as liczba from users where id_user="' . $params[0] .'"';
$zlicz = $db->query($query_zlicz)->fetch_array(MYSQLI_ASSOC);
$zlicz_suma = $zlicz['liczba'];
//echo $zlicz_suma;
if ($zlicz_suma==0){
	echo "<h1 class=ui header>Brak użytkownika</h1>";
}
else{
if($q = $db->query($query))
{
	$q_array = $q->fetch_array(MYSQLI_ASSOC);
	$nazwa = $q_array['username'];
	$email = $q_array['email'];
	$id = $q_array['id_user'];
}

if($zalogowany)
{
	$like_query = 'SELECT count(*) as czydal FROM oceny where dla_kogo="'.$params[0].'" and od_kogo="'.$_SESSION['id_uzytkownika'].'" and ocena="1"';
	$dislike_query = 'SELECT count(*) as czydal FROM oceny where dla_kogo="'.$params[0].'" and od_kogo="'.$_SESSION['id_uzytkownika'].'" and ocena="-1"';
	$z_l = $db->query($like_query);
	$likes = $z_l->fetch_array();
	$z_dl = $db->query($dislike_query);
	$dislikes = $z_dl->fetch_array();
	$dal_like = $likes['czydal'];
	$dal_dislike = $dislikes['czydal'];
	$dal = $dal_like + $dal_dislike;
}
if(isset($_FILES['avatar'])){
	 $file_name = $_FILES['avatar']['name'];
	 $file_tmp =$_FILES['avatar']['tmp_name'];
	 $file_name_array = explode('.',$file_name);
	 $file_name_extension = end($file_name_array);
	 $file_ext=strtolower($file_name_extension);
	 $avatar_dir = 'images/avatary';
	 $file = $avatar_dir . '/' . $nazwa . '.' . $file_ext;
	 move_uploaded_file($file_tmp,$file);
	 chmod($file, 0666);
	 if($file_ext != 'png'){
		 imagepng(imagecreatefromstring(file_get_contents($file)), $avatar_dir . '/' . $nazwa . '.png');
		 chmod($avatar_dir . '/' . $nazwa . '.png', 0666);
		 unlink($file);
	 }
	 $_SESSION['avatar_name'] = $nazwa . '.png';
	 header("Location: /tablica/userprofil/" . $id);

	 //move_uploaded_file($file_tmp,'images/avatary/'.$file_name);
}

if(isset($params[1]) && $params[1] == 'addlike')
{
	if($dal == 0)
	{
		$query_likes = 'INSERT INTO oceny VALUES(null, "'.$params[0].'", "'.$_SESSION['id_uzytkownika'].'", 1)';
		$db->query($query_likes);
	}
	else
	{
		$db->query('delete from oceny where od_kogo="' . $_SESSION['id_uzytkownika'] . '" and dla_kogo="' . $params[0] . '";');
		if($dal_dislike == 1)
		{
			$query_likes = 'INSERT INTO oceny VALUES(null, "'.$params[0].'", "'.$_SESSION['id_uzytkownika'].'", 1)';
			$db->query($query_likes);
		}
	}

	header('Location: /tablica/userprofil/' . $params[0]);
}

if(isset($params[1]) && $params[1] == 'addunlike')
{
	if($dal == 0)
		{
			$query_unlikes = 'INSERT INTO oceny VALUES(null, "'.$params[0].'", "'.$_SESSION['id_uzytkownika'].'", -1)';
			$db->query($query_unlikes);
		}
		else
		{
				$db->query('delete from oceny where od_kogo="' . $_SESSION['id_uzytkownika'] . '" and dla_kogo="' . $params[0] . '";');
				if($dal_like == 1)
				{
				$query_unlikes = 'INSERT INTO oceny VALUES(null, "'.$params[0].'", "'.$_SESSION['id_uzytkownika'].'", -1)';
				$db->query($query_unlikes);
			}
		}

		header('Location: /tablica/userprofil/' . $params[0]);
}

?>
<article class="ui piled segment">
<div class="ui two column stackable grid">
<!--<div style="float:left; text-align: center;">-->
	<div class="four wide column">

	<!--<div class="sub header">Avatar użytkownika:</div>-->
	<div style="margin-top: 10px;">
	<?php if($id == @$_SESSION['id_uzytkownika']){
		echo '<img class="ui small image centered"  onclick="$(\'.ui.modal.avatar\').modal(\'show\');" src="' . getUserAvatar($nazwa) . '"><br />';
	} else {
		echo '<img class="ui small image centered" src="' . getUserAvatar($nazwa) . '"><br />';
	}
	?>
	</div>
	<!-- NAZWA UZYTKOWNIKA -->
	<div class="sub header"><center> Użytkownik: <b><?php echo $nazwa; ?></b></div></b> </center>
	<br>

<center>
<?php
if($zalogowany)
	echo '<label>E-mail: </label><a href="mailto:'.$email.'">'.$email.'</a>';
	echo '<form action="./napisz/'.$params[0].'" method="post" class="ui reply form">
    <br/><button class="ui blue labeled submit icon button" type="submit">
      <i class="icon send"></i> Wiadomość
    </button>
<br />
</form>';
?>
<br>
<div class="ui buttons">
<?php
//ilosc łapek w gore
$lajki = $db->query('select count(*)  as c from oceny where dla_kogo="' . $params[0] . '" and ocena="1" limit 1;');
$i = $lajki->fetch_array(MYSQLI_ASSOC);
$ilosc_lajkow = $i['c'];
//echo $ilosc_lajkow;

//ilosc lapek w dol
$unlajki = $db->query('select count(*)  as u from oceny where dla_kogo="' . $params[0] . '" and ocena="-1" limit 1;');
$u = $unlajki->fetch_array(MYSQLI_ASSOC);
$ilosc_unlajkow = $u['u'];
//echo $ilosc_unlajkow;


if($zalogowany && $dal_like == 0)//zalogowany bez like
{
	echo '<a class="ui grey labeled icon button popup2" href="./userprofil/'.$params[0].'/addlike" data-tooltip="'.$ilosc_lajkow.' lubi to">
    <i class="left thumbs up icon"></i> Lubię to
	</a>
	';
}
else if($zalogowany) //zalogowany z like
{
	echo '<a class="ui red labeled icon button popup2" href="./userprofil/'.$params[0].'/addlike" data-tooltip="'.$ilosc_lajkow.' lubi to">
		<i class="left thumbs up icon"></i> Lubię to
	</a>
';
}
else //niezalogowany
{
	echo '<a class="ui disabled labeled grey icon button popup2" href="./userprofil/'.$params[0].'/addlike" data-tooltip="'.$ilosc_lajkow.' lubi to">
		<i class="left thumbs up icon"></i> Lubię to
	</a>
';
}

if($zalogowany && $dal_dislike == 0)
	{
		echo '<a class="ui grey right labeled icon button" href="./userprofil/'.$params[0].'/addunlike" data-tooltip="'.$ilosc_unlajkow.' nie lubi">
				<i class="right thumbs down icon"></i>Nie lubię
		</a>';
	}
	else if($zalogowany)
	{
		echo '<a class="ui red right labeled icon button" href="./userprofil/'.$params[0].'/addunlike" data-tooltip="'.$ilosc_unlajkow.' nie lubi">
			<i class="right thumbs down icon"></i>Nie lubię
		</a>';

	}
else {
	echo '<a class="ui disabled right labeled grey button" href="./userprofil/'.$params[0].'/addunlike" data-tooltip="'.$ilosc_unlajkow.' nie lubi">
		<i class="right thumbs down icon"></i>Nie lubię
	</a>';

}
	?>


</div>

<br>
<br>


<?php
if($zalogowany)
echo '
    <div class="ui red labeled submit icon button" onclick="$(\'.ui.modal.zglos\').modal(\'show\');">
      <i class="icon pencil"></i> Zgłoś użytkownika
    </div><br />';
?>
<br>

<?php
if($zalogowany)
echo '<div class="ui primary labeled submit icon button" onclick="$(\'.ui.modal.password\').modal(\'show\');">
		<i class="icon pencil"></i> Edytuj hasło
	</div>
<br />';
?>


</center>
</div>
<div class="twelve wide column">
<?php include 'userogloszenia.php' ?>
</div>
</div>
<div style="clear:both;"></div>

<div class="ui comments" style="margin-top: 50px; max-width: 100%;">
<h3 class="ui dividing header">Komentarze</b></h3>
<?php
if($zalogowany)
{
echo '<form method="POST" class="ui reply form" action="./dodaj_komentarz/'.$params[0].'">';
echo '<div class="required field">';
	echo '<div class="field">';
	echo '<textarea name="opis"></textarea>';
	echo '</div>';
	echo '<button type="submit" class="ui labeled blue icon button"><i class="save icon"></i>Dodaj komentarz</button>';
echo '</form>';
}

showCommentsRecursively($params[0]);

?>
</div>
</article>
<form method="POST" class="ui modal avatar form" enctype="multipart/form-data">
	<div class="ui header">
		Zmień avatar
	</div>
	<div class="content">
		<input name="avatar" type="file">

	</div>
	<div class="actions">
		<button type="submit "class="ui orange button">Zmień Avatar</button>
</div>
</form>

<form method="POST" class="ui modal zglos form" enctype="multipart/form-data" action="./zglos/<?=$id?>/zglos_usera">
	<div class="ui header">
		Zgłoś <?php echo $nazwa; ?>
	</div>
	<div class="content">
		<select class="ui dropdown" name="wybor_powodu">
			<option disabled selected value>Wybierz akcję</option>
			<option value="Obrazliwy avatar uzytkownika">Obraźliwy avatar użytkownika</option>
			<option value="Obrazliwy login uzytkownika">Obraźliwy login użytkownika</option>
			<option value="Spamowanie ogloszeniami">Spamowanie ogłoszeniami</option>
			<option value="Oszust">Oszust</option>
		</select>
	</div>
	<div class="actions">
		<button type="submit" class="ui red labeled icon button">
			<i class="pencil icon"></i>
			Zgłoś
		</button>
</div>
</form>

<form method="POST" class="ui modal password form" enctype="multipart/form-data" action="./edytujuser/change_password">
	<div class="ui header">
		Zmiana hasła
	</div>
	<div class="content">
		<div class="ui error message"></div>
		<div class="field">
			<label>Stare hasło</label>
			<input type="password" name="starehaslo" placeholder="Stare hasło" />
		</div>
		<div class="field">
			<label>Nowe hasło</label>
			<input type="password" name="haslo" placeholder="Nowe hasło" />
		</div>
		<div class="field">
			<label>Powtórz nowe hasło</label>
			<input type="password" name="haslo2" placeholder="Powtórz nowe hasło" />
		</div>
	</div>
  <div class="actions">
    <button type="submit" class="ui blue labeled icon button">
          <i class="pencil icon"></i>
          Zmień hasło użytkownika
    </button>
	</div>
</form>
<script>
$('.ui.modal.password')
	.form({
		fields: {
			starehaslo: {
				identifier: 'starehaslo',
				rules: [
					{
						type   : 'empty',
						prompt : 'Stare hasło nie może być puste'
					}
				]
			},
			haslo: {
				identifier: 'haslo',
				rules: [
					{
						type   : 'empty',
						prompt : 'Nowe hasło nie może być puste'
					},
					{
						type   : 'minLength[8]',
						prompt : 'Nowe hasło musi mieć minimum 8 znaków'
					}
				]
			},
			haslo2: {
				identifier: 'haslo2',
				rules: [
					{
						type   : 'empty',
						prompt : 'Hasła muszą być takie same'
					},
					{
						type   : 'match[haslo]',
						prompt : 'Hasła muszą być takie same'
					}
				]
			}
		}
	})
;
</script>
<?php } ?>

<?php
	$link_aktywna = 'wyswietl/';

	if(isset($params[0]))
	{
		$sort_array = explode(',', $params[0]);
		if($sort_array[0] == 'sortuj')
		{
			array_shift($params);
			$co = $sort_array[1];
			$jak = $sort_array[2];
			$query_order = $co . ' ' . $jak . ', ';
			$link_sortuj = 'sortuj,' . $co . ',' . $jak . '/';
		}
		else
		{
				$query_order = '';
				$link_sortuj = '';
		}
	}

	if(isset($_GET['filtruj']))
	{
		$link_filtruj = 'filtruj';
		foreach ($_GET as $k => $v) {
			$link_filtruj .= ',' . $v;
		}
		header("Location: /tablica/" . $link_aktywna . $link_sortuj . $link_filtruj);
	}
	elseif(isset($params[0]))
	{
		$params[0] = urldecode($params[0]);
		$filtr_array = explode(',', $params[0]);
		if($filtr_array[0] == 'filtruj')
		{
			$link_filtruj = $params[0] . '/';
			array_shift($params);
			$cena_od = $filtr_array[1];
			$cena_do = $filtr_array[2];
			$wojewodztwo = $filtr_array[3];
			$miasto = $filtr_array[4];
			$dodano = $filtr_array[5];
			$query_where_array = array();
			if(!empty($cena_od) && !empty($cena_do))
			{
				$query_where_array[] = 'cena between ' . $cena_od . ' and ' . $cena_do;
			}
			else if(!empty($cena_od))
			{
				$query_where_array[] = 'cena >= ' . $cena_od;
			}
			//else if(!empty($cena_do) || $cena_do == 0)
			else if(!empty($cena_do))
			{
				$query_where_array[] = 'cena <= ' . $cena_do;
			}

			if(!empty($dodano))
			{
				$query_where_array[] = 'data_wystawienia >= "' . $dodano . '"';
			}

			if(!empty($miasto))
			{
				$query_where_array[] = 'miasto LIKE "%' . $miasto . '%"';
			}

			if(!empty($wojewodztwo))
			{
				$query_where_array[] = 'wojewodztwa.nazwa_wojewodztwa = "' . $wojewodztwo . '"';
			}

			$query_where = implode(' and ', $query_where_array);
			if(count($query_where_array) > 0) $query_where = ' where ' . $query_where;
			//echo $query_where;
		}
		else
		{
				$query_where = '';
				$link_filtruj = '';
				$cena_od = '';
				$cena_do = '';
				$dodano = '';
				$wojewodztwo = '';
				$miasto = '';
				$link_filtruj = '';
		}
	}
	else
	{
		$link_filtruj = '';
	}

	$link_aktywna = $link_aktywna . $link_sortuj . $link_filtruj;

	$wszystkie_ogloszenia_query = 'select ogloszenie.*, users.username, users.email, kategorie.nazwa_kategorii, wojewodztwa.nazwa_wojewodztwa from ogloszenie join users on ogloszenie.fk_id_user = users.id_user join kategorie on ogloszenie.fk_id_kategoria=kategorie.id_kategoria join wojewodztwa on ogloszenie.fk_id_wojewodztwa=wojewodztwa.id_wojewodztwa'.$query_where;
	//echo $wszystkie_ogloszenia_query;
	$wszystkie_ogloszenia = $db->query($wszystkie_ogloszenia_query);
	$wszystkie = $wszystkie_ogloszenia->num_rows;

	$ilosc_na_strone = 6;
	$ilosc_stron = ceil($wszystkie / $ilosc_na_strone);

	$strona = !empty($params[0]) && is_numeric($params[0]) ? $params[0] : 1;

	$ogloszenia_query = 'select ogloszenie.*, users.username, users.email, kategorie.nazwa_kategorii, wojewodztwa.nazwa_wojewodztwa from ogloszenie join users on ogloszenie.fk_id_user = users.id_user join kategorie on ogloszenie.fk_id_kategoria=kategorie.id_kategoria join wojewodztwa on ogloszenie.fk_id_wojewodztwa=wojewodztwa.id_wojewodztwa'.$query_where.' order by ' . $query_order . 'data_wystawienia desc, id_ogloszenie desc limit ' . (($strona - 1) * $ilosc_na_strone) . ', ' . ($ilosc_na_strone);
	//echo $ogloszenia_query;
	$ogloszenia = $db->query($ogloszenia_query);

?>
<div class="ui horizontal menu">
<!--<div class="ui labeled icon dropdown item">-->
		<div class="ui dropdown item">Sortuj
		<i class="dropdown icon"></i>
		<!--<div class="ui vertical menu">-->
			<div class="menu">
				<a href="./wyswietl/sortuj,cena,asc/<?php echo $link_filtruj; ?>" class="item">Cena rosnąco</a>
				<a href="./wyswietl/sortuj,cena,desc/<?php echo $link_filtruj; ?>" class="item">Cena malejąco</a>
				<a href="./wyswietl/sortuj,nazwa,asc/<?php echo $link_filtruj; ?>" class="item">Nazwa A-Z</a>
				<a href="./wyswietl/sortuj,nazwa,desc/<?php echo $link_filtruj; ?>" class="item">Nazwa Z-A</a>
				<a href="./wyswietl/sortuj,data_wystawienia,asc/<?php echo $link_filtruj; ?>" class="item">Data rosnąco</a>
				<a href="./wyswietl/sortuj,data_wystawienia,desc/<?php echo $link_filtruj; ?>" class="item">Data malejąco</a>
			</div>
		</div>
		<div class="link item">
			<div class="filtruj">Filtruj <i class="dropdown icon"></i></div>
			<div class="ui flowing popup top left transition hidden" style="width: 800px;">
					<form class="ui three column divided center aligned grid form" action="<?php echo $link_aktywna; ?>" method="get">
				    <div class="column">
							<div class="field">
								<label>Cena</label>
								<input type="number" name="cena_od" placeholder="od" value="<?php echo $cena_od; ?>" />
							</div>
							<div class="field">
								<input type="number" name="cena_do" placeholder="do" value="<?php echo $cena_do; ?>" />
							</div>
				    </div>
				    <div class="column">
							<div class="field">
							<label>Lokalizacja</label>
								<select class="ui dropdown" name="wojewodztwo">
									<option value=""></option>
									<?php
										foreach($db->query('select * from wojewodztwa') as $w)
										{
											echo '<option value="'.$w['nazwa_wojewodztwa'].'"';
											if($wojewodztwo == $w['nazwa_wojewodztwa'])
												echo ' selected="selected"';
											echo '>'.$w['nazwa_wojewodztwa'].'</option>';
										}
									?>
								</select>
							</div>
							<div class="field">
								<input type="text" name="miejscowosc" placeholder="Miejscowość" value="<?php echo $miasto; ?>" />
							</div>
				    </div>
				    <div class="column">
							<div class="field">
								<label>Dodano</label>
								<input type="date" name="dodano" placeholder="Dodano" value="<?php echo $dodano; ?>" />
							</div>
							<div class="field">
								<input class="ui grey button" type="submit" name="filtruj" value="Filtruj" style="margin-left:7.5%;margin-top: 2.3%;"/>
							</div>
				    </div>
					</form>
			</div>



		</div>
</div>



<?php

if($wszystkie > 0 && $strona <= $ilosc_stron)
{
		echo '<div class="ui two column stackable grid">';

		include 'ogloszenia.php';

		showPagination($ilosc_stron, $strona, $link_aktywna);

		echo '</div>';

}
else
{
	echo '<div class="ui segment"><h1>Brak wyników</h1></div>';
}
?>

<script>
$('.filtruj')
  .popup({
    inline     : true,
    hoverable  : true,
		on					:	'click',
    //position   : 'bottom left',
    delay: {
      show: 300,
      hide: 800
    }
  })
;
</script>
